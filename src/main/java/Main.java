public class Main {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsik");
        Cat murzic = new Cat("murzic");

        Animal[] animals = new Animal[]{barsik, murzic};
        for (Animal animal : animals) {
            Cat cat = (Cat) animal;
            System.out.println(cat.toString());		//почему тут выведет null????????????????
        }
    }
}
